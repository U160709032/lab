package yusuf.main;

import java.util.ArrayList;
import yusuf.shapes3d.*;

public class Test3D 
{

	public static void main(String[] args) 
	{
       
	

        ArrayList<Cylinder> cylinders = new ArrayList<>();
        Cylinder cylinder = new Cylinder();

        cylinders.add(cylinder);
        cylinders.add(new Cylinder(6,7));
        System.out.println("Cylinders info: " + cylinders);
  

        ArrayList<Cube> cubes = new ArrayList<>();
        Cube cube = new Cube();

        cubes.add(cube);
        cubes.add(new Cube(4));
        System.out.println("Cubes info: " + cubes);
	}
    
}