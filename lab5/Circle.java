
public class Circle {
	public int radius;
	
	public double area() {
		return 3.14 * radius * radius;
	}
	public double perimeter() {
		return 3.14  * 2 * radius;
}}
