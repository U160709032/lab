public class Main {
	public static void main(String[] args) {
	Main rectangle = new Main();
	rectangle.rec();
	rectangle.circ();
	}
	public  void rec() {
		Rectangle rectA = new Rectangle();
		rectA.sideA = 5;
		rectA.sideB = 6;
		System.out.println("Area of the Rectangle: " + rectA.area());
		System.out.println("Perimeter of the Rectangle : " + rectA.perimeter() + "\n");
	}
	public  void circ() {
		Circle circA = new Circle();
		circA.radius = 10;
		System.out.println("Area of the Circle : " + circA.area());
		System.out.println("Perimeter of the Circle : " + circA.perimeter() + "\n");
}}
