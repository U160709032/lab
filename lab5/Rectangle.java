public class Rectangle {
	int sideA;
	int sideB;
	
	public int area() {
		return sideA * sideB;
	}
	
	public int perimeter() {
		return 2 * sideA + sideB;
}}
