package generics;

import java.util.ArrayList;
import java.util.List;

public class Exercise1 {
	public static void main(String [] args) { 
	  String[] strs = {"a", "b", "c","d"};
	  List<String> strList = new ArrayList<>();
	  addToCollection(strs,strList);


	  Integer[] ints = {1, 2, 3, 4};
	  List<Integer> intList = new ArrayList<>();
	  addToCollection(ints,intList);
	  
	}

	private static <T> void addToCollection(T[] array, List<T> list) {
	for(T elements: array) {
		list.add(elements);
		}	
	}
}
