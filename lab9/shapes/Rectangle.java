package shapes;

public class Rectangle extends Shape{

	int length;
	int width;
	
	public Rectangle(int l, int w) {
		this.length = l;
		this.width = w;
	}
	
	public double area() {
		return length * width; 
	}
}
