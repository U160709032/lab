public class Q5 {
	
	private static double calculateAverage(int[][] values) {
		int sum = 0, element = 0;
		
		for (int i = 0; i < values.length; i++) {
			for (int j = 0; j < values.length; j++) {
				sum += values[i][j];
				element++;
			}
		}
		
		return sum/element;
	}
}
